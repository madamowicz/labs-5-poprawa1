﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5.Kontroler.Contract
{
    public interface IKontroler
    {
        void Programuj(string program);
        void ZglosZakonczenie();
    }
}
