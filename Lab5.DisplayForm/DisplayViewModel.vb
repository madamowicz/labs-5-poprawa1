﻿Imports System.ComponentModel

Public Class DisplayViewModel
    Implements INotifyPropertyChanged

    Private _text As String

    Public Sub New()
        Text = ""
    End Sub

    Public Property Text As String
        Get
            Return _text
        End Get
        Set(value As String)
            _text = value
            OnPropertyChanged("Text")
        End Set
    End Property

    Public Sub OnPropertyChanged(ByVal propertyName As String)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Public Event PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Implements INotifyPropertyChanged.PropertyChanged
End Class
