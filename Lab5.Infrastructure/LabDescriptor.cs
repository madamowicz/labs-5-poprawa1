﻿using System;
using System.Reflection;
using PK.Container;
using Lab5.Container;
using Lab5.Kontroler.Contract;
using Lab5.Kontroler.Implementation;
using Lab5.Display.Contract;
using Lab5.Display.Implementation;

namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(Lab5.Container.Kontener);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IKontroler));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Kontroler.Implementation.Kontroler));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(IEkran));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(Ekran));

        #endregion
    }
}
