﻿using System;
using PK.Container;
using Lab5.Kontroler.Contract;
using Lab5.Kontroler.Implementation;
using Lab5.Display.Contract;
using Lab5.Display.Implementation;
using Lab5.Container;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            IContainer container = new Container.Kontener();
            container.Register(typeof(Display.Implementation.Ekran));
            container.Register(typeof(Kontroler.Implementation.Kontroler));
            return container;         
        }
    }
}
